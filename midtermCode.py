#!/usr/bin/env python
#****Note: The move-it python interface tutorial code was referenced and used for different portions of this code

from __future__ import print_function

#packages important to control the robot in Gazebo using MoveIt-Python interface
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

#condition that enables code to work with different versions of python based on what packages it can import
try:
    from math import pi, tau, dist, fabs, cos
except:
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

#tests if tolerance of measurements of identical and opposite orientations is within a certain bound
def all_close(goal, actual, tolerance):
    
    if type(goal) is list:
        # returns False if measurement is incorrect, i.e. if difference is above the desired tolerance
        for index in range(len(goal)):
            if abs(actual[index] - goal[index]) > tolerance:
                return False

    elif type(goal) is geometry_msgs.msg.PoseStamped:
        return all_close(goal.pose, actual.pose, tolerance)

    # establishes whether both the distance in space and the angle between the orientation measurement and the vertical at a specific pose goal is below the tolerance specified
    elif type(goal) is geometry_msgs.msg.Pose:
        x0, y0, z0, qx0, qy0, qz0, qw0 = pose_to_list(actual)
        x1, y1, z1, qx1, qy1, qz1, qw1 = pose_to_list(goal)
        d = dist((x1, y1, z1), (x0, y0, z0))

        cos_phi_half = fabs(qx0 * qx1 + qy0 * qy1 + qz0 * qz1 + qw0 * qw1)
        return d <= tolerance and cos_phi_half >= cos(tolerance / 2.0)

    return True


#the myProject class is created so that functions can be utilized to affect the robot's motion and position and space in the main class
class myProject(object):

    #this function initializes the relevant objects and nodes so MoveIt can communicate with the robot in the Gazebo simulation
    def __init__(self):
        super(myProject, self).__init__()

        #the moveit_commander is initialized so the code can use MoveIt to create joint states and pose goals in the following functions
        moveit_commander.roscpp_initialize(sys.argv)
        
        #the rospy node is initialized to allow for communication with ROS
        rospy.init_node("node", anonymous=True)

        #A moveit_commander object, called robot, is made
        robot = moveit_commander.RobotCommander()

        scene = moveit_commander.PlanningSceneInterface()

        #the "manipulator" group name allows the commander to use the arm joints in the UR5e robot specifically
        group_name = "manipulator"
        move_group = moveit_commander.MoveGroupCommander(group_name)

        planning_frame = move_group.get_planning_frame()

        eef_link = move_group.get_end_effector_link()

        group_names = robot.get_group_names()
    
        self.robot = robot
        self.scene = scene
        self.move_group = move_group
        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names


    #this function allows one to specify the revolute joint angles for the robot being controlled.
    def go_to_joint_state(self):
        move_group = self.move_group

        joint_goal = move_group.get_current_joint_values()

        #each joint is given an initial angle that is relative to zero angles in the base configuration of the robot. Tau is established earlier as being 2pi
        joint_goal[0] = -tau / 4
        joint_goal[1] = -tau / 4
        joint_goal[2] = -tau / 4
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = 0

        move_group.go(joint_goal, wait=True)

        move_group.stop()

        current_joints = move_group.get_current_joint_values()
        return all_close(joint_goal, current_joints, 0.01)

    #this function uses operational space to control the robot, where poses are specified based on the coordinates of the end effector of the UR5e robot
    def go_to_pose_goal(self, ori, posX, posY, posZ):
 
        move_group = self.move_group
        
        #the arguments of orientation and the x, y, and z positions are then set to the following functions
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.orientation.w = ori
        pose_goal.position.x = posX
        pose_goal.position.y = posY
        pose_goal.position.z = posZ

        move_group.set_pose_target(pose_goal)

        success = move_group.go(wait=True)
       
        move_group.stop()
        
        move_group.clear_pose_targets()

        current_pose = self.move_group.get_current_pose().pose
        return all_close(pose_goal, current_pose, 0.01)


#the functions are called in the main, where the joint state is first called in order to control the robots initial configuration at the top right of the first initial E.
def main():
    try:
        proj = myProject()
        proj.go_to_joint_state()

        #pose goals for letter E utilizing the relevant coordinates
        proj.go_to_pose_goal(1, .2, .4, 0.6)
        proj.go_to_pose_goal(1, .2, .2, 0.6)
        proj.go_to_pose_goal(1, .2, .2, .45)
        proj.go_to_pose_goal(1, .2, .325, .45)
        proj.go_to_pose_goal(1, .2, .2, .45)
        proj.go_to_pose_goal(1, .2, .2, .3)
        proj.go_to_pose_goal(1, .2, .4, .3)

        #pose goals for letter H
        proj.go_to_pose_goal(1, .2, .2, 0.6)
        proj.go_to_pose_goal(1, .2, .2, 0.3)
        proj.go_to_pose_goal(1, .2, .2, .45)
        proj.go_to_pose_goal(1, .2, .4, .45)
        proj.go_to_pose_goal(1, .2, .4, .6)
        proj.go_to_pose_goal(1, .2, .4, .3)

        #pose goals for letter R
        proj.go_to_pose_goal(1, .2, .2, 0.6)
        proj.go_to_pose_goal(1, .2, .2, 0.3)
        proj.go_to_pose_goal(1, .2, .2, .6)
        proj.go_to_pose_goal(1, .2, .325, .6)
        proj.go_to_pose_goal(1, .2, .375, .55)
        proj.go_to_pose_goal(1, .2, .375, .5)
        proj.go_to_pose_goal(1, .2, .325, .45)
        proj.go_to_pose_goal(1, .2, .2, .45)
        proj.go_to_pose_goal(1, .2, .3, .45)
        proj.go_to_pose_goal(1, .2, .375, .3)

    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return


if __name__ == "__main__":
    main()