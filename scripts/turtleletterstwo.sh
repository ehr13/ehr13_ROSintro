#!/usr/bin/bash

rosservice call /spawn 6 2.5 0 'turtle2'
rosservice call /kill 'turtle1'
rosservice call /spawn 4 2.5 0 'turtle1'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 1.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[-1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 1.5, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call /turtle2/set_pen 180 180 180 4 0

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 3.0, 0.0]' '[0.0, 0.0, 0.0]'

